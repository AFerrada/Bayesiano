from obspy import read
import numpy as np
import pandas as pd
import datetime as dt
from obspy.core import UTCDateTime as utc

st1 = read("Datos/2018-04-10T09:43:00.mseed")
st2 = read("Datos/2018-04-13T09:43:00.mseed")
st3 = read("Datos/2018-04-16T11:49:00.mseed")
st4 = read("Datos/2018-04-21T11:49:00.mseed")
st5 = read("Datos/2018-04-26T04:38:00.mseed")
st6 = read("Datos/2018-04-30T04:38:00.mseed")
st7 = read("Datos/2018-05-04T19:30:00.mseed")
st8 = read("Datos/2018-05-15T19:30:00.mseed")
st9 = read("Datos/2018-05-27T15:07:00.mseed")
st10 = read("Datos/2018-06-06T15:07:00.mseed")
st11 = read("Datos/2018-06-16T00:15:00.mseed")
st12 = read("Datos/2018-06-21T00:15:00.mseed")
st13 = read("Datos/2018-06-26T04:04:00.mseed")
st14 = read("Datos/2018-06-29T04:04:00.mseed")
st15 = read("Datos/2018-07-01T15:02:00.mseed")
st16 = read("Datos/2018-07-11T15:02:00.mseed")
st17 = read("Datos/2018-07-21T12:04:00.mseed")
st18 = read("Datos/2018-07-25T12:04:00.mseed")
st19 = read("Datos/2018-08-01T05:40:00.mseed")
st20 = read("Datos/2018-08-06T05:40:00.mseed")

stVals = [st1, st2, st3, st4, st5,
          st6, st7, st8, st9, st10,
          st11, st12, st13, st14, st15,
          st16, st17, st18, st19, st20]



def takeData(st, timeStart, timeFinish, df):

    for element in st:

        element.resample(1.0)

        actualTime = element.stats.starttime
        deltaTime = (element.stats.endtime - element.stats.starttime) / element.stats.npts

        dfAux = pd.DataFrame()
        dfAux = pd.concat([dfAux,pd.DataFrame(element.data, columns=["values"])],   axis=0)
        dfAux = pd.concat([dfAux, pd.DataFrame([(element.stats.channel) for x in range(element.stats.npts)], columns=["stationAndSignal"])],  axis=1)

        array = []
        for values in element.data:

            if(actualTime > timeStart and actualTime < timeFinish):
                array.append(1)
            else:
                array.append(0)

            actualTime = actualTime + deltaTime

        dfAux = pd.concat([dfAux, pd.DataFrame(array, columns=["isSism"])],  axis=1)

        ##print(str(element.stats.npts) + "---" + element.stats.station +"-"+ element.stats.channel )
        df = pd.concat([df, dfAux])

    return df


dfA = pd.DataFrame()

timeSt1 = utc(year=2018,month=4,day=10,hour=10,minute=19,second=34)
timeSt2 = utc(year=2018,month=4,day=16,hour=12,minute=31,second=35)
timeSt3 = utc(year=2018,month=4,day=26,hour=05,minute=32,second=49)
timeSt4 = utc(year=2018,month=5,day=04,hour=20,minute=25,second=34)
timeSt5 = utc(year=2018,month=5,day=27,hour=15,minute=18,second=01)
timeSt6 = utc(year=2018,month=6,day=16,hour=01,minute=01,second=07)
timeSt7 = utc(year=2018,month=6,day=26,hour=04,minute=32,second=12)
timeSt8 = utc(year=2018,month=7,day=01,hour=15,minute=28,second=41)
timeSt9 = utc(year=2018,month=7,day=21,hour=12,minute=49,second=21)
timeSt10 = utc(year=2018,month=8,day=01,hour=06,minute=19,second=05)
timeSTNULL = utc(year=1, month=1, day=1)
timeDeltaNull = dt.timedelta(seconds=1)

tS = [timeSt1, timeSTNULL, timeSt2, timeSTNULL, timeSt3, timeSTNULL,
      timeSt4, timeSTNULL, timeSt5, timeSTNULL, timeSt6, timeSTNULL,
      timeSt7, timeSTNULL, timeSt8, timeSTNULL, timeSt9, timeSTNULL,
      timeSt10, timeSTNULL]

timeDeltaFi = [dt.timedelta(minutes=2, seconds=30), timeDeltaNull, dt.timedelta(minutes=2, seconds=30), timeDeltaNull, dt.timedelta(minutes=2), timeDeltaNull,
              dt.timedelta(minutes=2), timeDeltaNull, dt.timedelta(minutes=2, seconds=30), timeDeltaNull, dt.timedelta(minutes=3), timeDeltaNull,
              dt.timedelta(minutes=2, seconds=30), timeDeltaNull, dt.timedelta(minutes=2, seconds=30), timeDeltaNull, dt.timedelta(minutes=3), timeDeltaNull,
              dt.timedelta(minutes=3), timeDeltaNull]

timeFi = [tS[i] + timeDeltaFi[i] for i in range(len(tS))]

i = 0

for st in stVals:
    dfA = takeData(st,tS[i],timeFi[i], dfA)
    i+=1
    print("se a evaluado " + str(i))


dfA = dfA.reset_index(drop=True)

dfA.to_csv("Datos/resultados.csv")
